package com.rjproject.controllers.user;

import com.rjproject.dto.UserDto;
import com.rjproject.entities.User;
import com.rjproject.service.post.PostService;
import com.rjproject.service.user.UserService;
import com.rjproject.service.user.UserServiceForView;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/user")
public class UserPagesController {

    private static final Logger logger = Logger.getLogger(UserPagesController.class);

    @Autowired
    private UserServiceForView userServiceForView;

    @GetMapping(path = {"/{username}"})
    public ModelAndView showMainPage(@PathVariable(value = "username") String username) {

        UserDto requestedUser = userServiceForView.findByUserName(username);

        logger.info("showing user's " + username + " personal page");

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("user", requestedUser);
        modelAndView.setViewName("userTemplates/userPage");

        return modelAndView;

    }

}
