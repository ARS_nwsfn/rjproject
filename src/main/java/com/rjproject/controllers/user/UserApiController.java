package com.rjproject.controllers.user;

import com.rjproject.dto.PostCommentDto;
import com.rjproject.dto.PostDto;
import com.rjproject.service.post.PostServiceForView;
import com.rjproject.service.security.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;


@Controller
@RequestMapping("/api/user")
public class UserApiController {

    @Autowired
    PostServiceForView postServiceForView;

    @Autowired
    SecurityService securityService;

    @GetMapping(value = "/{username}/getPosts")
    public ModelAndView getPosts( @PathVariable("username") String username) {

        List<PostDto> posts = postServiceForView.getPostsByUser(username) ;
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("posts",posts);
        modelAndView.addObject("currentUser",securityService.getLoggedInUser());
        modelAndView.setViewName("fragments/posts :: posts");
        return modelAndView;
    }

    @GetMapping(value = "/{username}/getComments")
    public ModelAndView getComments(@PathVariable("username") String username) {

        List<PostCommentDto> comments = postServiceForView.getCommentsByUser(username);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("comments",comments);
        modelAndView.setViewName("fragments/comments :: userComments");
        return modelAndView;
    }
}
