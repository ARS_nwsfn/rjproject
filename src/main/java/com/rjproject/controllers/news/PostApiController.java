package com.rjproject.controllers.news;

import com.rjproject.dto.PostDto;
import com.rjproject.service.post.PostServiceForView;
import com.rjproject.service.security.SecurityService;
import com.rjproject.validator.PostForm;
import com.rjproject.validator.PostValidator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@RequestMapping("/api/post")
public class PostApiController {

    private static final Logger logger = Logger.getLogger(PostApiController.class);

    @Autowired
    private PostServiceForView postServiceForView;

    @Autowired
    private PostValidator postValidator;

    @Autowired
    private SecurityService securityService;

    @PostMapping(path = {"/create"})
    public String createNewPost(@ModelAttribute("newPost") @Valid PostForm postForm, BindingResult bindingResult) {

        String currentUser = securityService.getLoggedUsername();
        logger.info("user: " + currentUser + " wants to create new post");
        logger.info("post topic: " + postForm.getTopic());
        logger.info("post body: " + postForm.getPostBody());
        postValidator.validate(postForm, bindingResult);

        if (bindingResult.hasErrors()) {
            System.out.println("------------>>" + bindingResult.getAllErrors().toString());
            return "postTemplates/createPost";
        }
        PostDto post = postServiceForView.createNewPost(postForm.getTopic(), postForm.getPostBody(),currentUser);

        return "redirect:/post/" + post.getPostId();
    }
}
