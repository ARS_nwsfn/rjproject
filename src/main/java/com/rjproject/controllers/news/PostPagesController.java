package com.rjproject.controllers.news;

import com.rjproject.dto.PostDto;
import com.rjproject.dto.UserDto;
import com.rjproject.entities.Post;
import com.rjproject.service.post.PostService;
import com.rjproject.service.post.PostServiceForView;
import com.rjproject.service.post.PostServiceUtil;
import com.rjproject.service.security.SecurityService;
import com.rjproject.service.user.UserService;
import com.rjproject.validator.PostForm;
import com.rjproject.validator.PostValidator;
import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/")
public class PostPagesController {

    private static final Logger logger = Logger.getLogger(PostPagesController.class);

    @Autowired
    private PostService postService;

    @Autowired
    private PostServiceForView postServiceForView;

    @Autowired
    private UserService userService;

    @Autowired
    private PostValidator postValidator;

    @Autowired
    private SecurityService securityService;

    @Autowired
    ModelMapper modelMapper;

    @GetMapping(path = {"/posts"})
    public ModelAndView showMainPage(@RequestParam(value = "page", required = false) Integer page) {

        UserDto currentUser = securityService.getLoggedInUser();

        Long totalPostNumber = postService.getTotalPostCount();
        logger.info("total post number = " + totalPostNumber);

        int lastPageNumber = (int) (Math.ceil(totalPostNumber / 4.));
        logger.info("last page number = " + lastPageNumber);

        page = PostServiceUtil.validatePageNumber(page == null ? 0 : page, totalPostNumber);
        logger.info("newsletter shown on page " + page);

        ArrayList<PostDto> posts = (ArrayList<PostDto>) postServiceForView.getPostsByPage(page);
        logger.info("Showing news page");

        List<Post> postList = postService.getAllPosts();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("posts", posts);
        modelAndView.addObject("lastPageNumber", lastPageNumber);
        modelAndView.addObject("currentPageNumber", page);
        modelAndView.addObject("pageStart", PostServiceUtil.getPaginationPageStart(page));
        modelAndView.addObject("pageEnd", PostServiceUtil.getPaginationPageEnd(page, lastPageNumber));
        modelAndView.addObject("currentUser", currentUser);
        modelAndView.setViewName("News");

        return modelAndView;

    }

    @GetMapping(path = {"/post/{id}"})
    public ModelAndView showPost(@PathVariable(value = "id") Integer id) {

        UserDto currentUser = securityService.getLoggedInUser();
        PostDto postDto = postServiceForView.getPostById(id);
        logger.info("showing post № " + id + " to a USER: " + currentUser.getUsername());

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("post", postDto);
        modelAndView.addObject("currentUser", currentUser);
        modelAndView.setViewName("postTemplates/specificPost");

        return modelAndView;
    }

    @GetMapping(path = {"/post/create"})
    public ModelAndView showPostCreationForm() {

        UserDto currentUser = securityService.getLoggedInUser();
        logger.info("USER: " + currentUser.getUsername() + " want to create post");


        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("newPost", new PostForm());
        modelAndView.setViewName("postTemplates/createPost");

        return modelAndView;

    }

}
