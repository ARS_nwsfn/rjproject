package com.rjproject.controllers.news;

import com.fasterxml.jackson.annotation.JsonView;
import com.rjproject.dto.PostCommentDto;
import com.rjproject.dto.PostDto;
import com.rjproject.dto.PostLikeDto;
import com.rjproject.entities.Post;
import com.rjproject.entities.PostComment;
import com.rjproject.entities.PostLike;
import com.rjproject.model.AjaxLikePostRequestBody;
import com.rjproject.jsonview.Views;
import com.rjproject.model.AjaxCommentsResponseBody;
import com.rjproject.model.AjaxLikeResponseBody;
import com.rjproject.model.AjaxSaveCommentRequestBody;
import com.rjproject.service.post.PostService;
import com.rjproject.service.post.PostServiceForView;
import com.rjproject.service.post.PostServiceUtil;
import com.rjproject.service.security.SecurityService;
import com.rjproject.service.user.UserService;
import com.rjproject.validator.PostForm;
import com.rjproject.validator.PostValidator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Calendar;
import java.util.Set;

@RestController
@RequestMapping("/api/post")
public class RestPostApi {

    private static final Logger logger = Logger.getLogger(RestPostApi.class);

    @Autowired
    private PostService postService;

    @Autowired
    private UserService userService;

    @Autowired
    private PostServiceForView postServiceForView;


    @JsonView(Views.Public.class)
    @PostMapping(value = "/{ID}/like")
    public AjaxLikeResponseBody likePost(@RequestBody AjaxLikePostRequestBody postLikeRequest, @PathVariable("ID") int id) {

        AjaxLikeResponseBody result = new AjaxLikeResponseBody();

        result.setCode("200");
        result.setMsg("rating changed successfully");
        result.setResult(postService.likePost(id, postLikeRequest.getRating()));
        return result;

    }

    @Transactional
    @JsonView(Views.Public.class)
    @GetMapping(value = "/{ID}/comments")
    public AjaxCommentsResponseBody getPostComments(@PathVariable("ID") int id) {

        AjaxCommentsResponseBody result = new AjaxCommentsResponseBody();

        PostDto post = postServiceForView.getPostById(id);
        Set<PostCommentDto> comments = post.getComments();

        if (!comments.isEmpty()) {
            result.setCode("200");
            result.setMsg("comments sent to user");
            result.setResult(comments);
        } else {
            result.setCode("204");
            result.setMsg("No comments!");
        }

        return result;

    }

    @Transactional
    @JsonView(Views.Public.class)
    @PostMapping(value = "/{ID}/add-comment")
    public AjaxCommentsResponseBody createComment(@RequestBody AjaxSaveCommentRequestBody addCommentRequest, @PathVariable("ID") int id) {

        AjaxCommentsResponseBody result = new AjaxCommentsResponseBody();

        logger.info("parrent comment id: " + addCommentRequest.getCommentId() + " post id: " + id + " comment: " + addCommentRequest.getCommentText());

        result.setCode("200");
        result.setMsg("comment saved");
        Set<PostCommentDto> comments = postServiceForView.addComment(id, addCommentRequest.getCommentId(), addCommentRequest.getCommentText()).getComments();
        result.setResult(comments);

        return result;

    }

}
