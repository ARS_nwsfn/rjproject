package com.rjproject.controllers.admin;

import com.rjproject.dto.UserDto;
import com.rjproject.service.user.UserUtilService;
import com.rjproject.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.apache.log4j.Logger;

import java.util.List;

@Controller
@RequestMapping("/admin")
public class AdminViewController {

    private static final Logger logger = Logger.getLogger(AdminViewController.class);

    @Autowired
    private UserService userService;

    @GetMapping
    @Transactional
    public ModelAndView showAdminPage() {

        logger.info("admin page shown");

        List<UserDto> users = userService.getAllUsersDto();

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("users",users);
        modelAndView.addObject("possibleRoles", UserUtilService.getAllPossibleRoles());
        modelAndView.setViewName("admin");

        return modelAndView;

    }
}
