package com.rjproject.controllers.admin;

import com.rjproject.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.apache.log4j.Logger;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/api/admin")
public class AdminApiController {

    private static final Logger logger = Logger.getLogger(AdminApiController.class);

    @Autowired
    private UserService userService;


    @DeleteMapping("/{username}/{role}")
    public String deleteUserRole(@PathVariable("username") String username, @PathVariable("role") String roleToDelete, RedirectAttributes redirectAtts) {

        logger.info("role to delete:" + roleToDelete + " from user:" + username);
        userService.deleteRole(username, roleToDelete);

        redirectAtts.addFlashAttribute("message", "role was deleted");
        return "redirect:/admin";
    }

    @PutMapping("/{username}/{role}")
    public String addUserRole(@PathVariable("username") String username, @PathVariable("role") String roleToDelete, RedirectAttributes redirectAtts) {

        logger.info("role to add:" + roleToDelete + " to user:" + username);
        userService.addRole(username,roleToDelete);

        redirectAtts.addFlashAttribute("message", "role was added");
        return "redirect:/admin";
    }
}
