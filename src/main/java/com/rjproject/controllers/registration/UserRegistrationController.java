package com.rjproject.controllers.registration;

import com.rjproject.dto.UserDto;
import com.rjproject.service.security.SecurityService;
import com.rjproject.service.user.UserService;
import com.rjproject.validator.UserForm;
import com.rjproject.validator.UserValidator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
@RequestMapping("/registration")
@PropertySource("classpath:/validation.properties")
public class UserRegistrationController {

    private static final Logger logger = Logger.getLogger(UserRegistrationController.class);

    @Autowired
    Environment env;

    @Autowired
    private UserService userService;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private UserValidator userValidator;

    @GetMapping
    public ModelAndView registration() {

        logger.info("registration page shown");
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("userForm", new UserForm());
        modelAndView.setViewName("registration");

        return modelAndView;
    }

    @PostMapping
    public String registration(@ModelAttribute("userForm") @Valid UserForm userForm, BindingResult bindingResult) {
        userValidator.validate(userForm, bindingResult);

        if (bindingResult.hasErrors()) {
            return "registration";
        }

        userService.saveUser(userForm.getUsername(), userForm.getPassword());

        securityService.autoLogin(userForm.getUsername(), userForm.getPassword());

        return "redirect:/";
    }

}