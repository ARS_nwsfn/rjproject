package com.rjproject.service.user;


import com.rjproject.dto.PostLikeDto;
import com.rjproject.dto.UserDto;
import com.rjproject.entities.User;
import com.rjproject.entities.UserRole;
import com.rjproject.entities.enums.Role;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public final class UserUtilService {

    private static BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    public static void addSpecificUserRole(User user, String role) {
        if (checkIfUserHaveRole(user, role)) {
            return;
        }
        Set<UserRole> userRoles = user.getUserRole();
        UserRole newRole = new UserRole();
        newRole.setUser(user);
        newRole.setRole(role);
        userRoles.add(newRole);
        user.setUserRole(userRoles);
    }

    public static boolean checkIfUserHaveRole(User user, String role) {
        return user.getUserRole().stream().anyMatch(userRole -> userRole.getRole().equals(role));
    }

    public static boolean checkIfUserHaveRole(UserDto user, String role) {
        return user.getUserRole().stream().anyMatch(userRole -> userRole.getRole().equals(role));
    }

    public static String encodePassword(String password) {
        return passwordEncoder.encode(password);
    }

    public static Set<String> getAllPossibleRoles() {
        return Stream.of(Role.values())
                .map(Role::name)
                .collect(Collectors.toSet());
    }

    public static int CheckIfUserLikePost(String name, HashSet<PostLikeDto> likes) {
        return likes.stream().filter(postLike -> postLike.getUser().getUsername().equals(name))
                .findAny()
                .map(postLike -> postLike.getLike() ? 1 : -1)
                .orElse(0);
    }


}
