package com.rjproject.service.user;

import com.rjproject.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserServiceForViewImpl implements UserServiceForView {

    @Autowired
    UserService userService;

    @Transactional
    public UserDto findByUserName(String name){
        return userService.convertToDto(userService.findByUserName(name));
    }
}
