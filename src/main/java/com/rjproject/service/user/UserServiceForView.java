package com.rjproject.service.user;

import com.rjproject.dto.UserDto;

public interface UserServiceForView {
    public UserDto findByUserName(String name);
}
