package com.rjproject.service.user;

import com.rjproject.dto.UserDto;
import com.rjproject.entities.User;
import com.rjproject.entities.UserRole;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserService extends UserDetailsService {

    void saveUser(User user);

    void saveUser(String username, String password);

    void updateUser(User user);

    List<User> getAllUsers();

    List<UserDto> getAllUsersDto();

    List<UserRole> getAllRoles();

    void deleteRole(String username, String role);

    void addRole(String username, String role);

    User findByUserName(String name);

    UserDto convertToDto(User user);

    User convertToEntity(UserDto userDto);


}