package com.rjproject.service.user;

import com.rjproject.dao.User.UserDao;
import com.rjproject.dao.User.UserRoleDao;
import com.rjproject.dto.UserDto;
import com.rjproject.entities.User;
import com.rjproject.entities.UserRole;
import com.rjproject.service.security.SecurityService;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


@Service
public class UserServiceImp implements UserService {

    Logger logger = LoggerFactory.getLogger(UserServiceImp.class);

    @Autowired
    private UserDao userDao;

    @Autowired
    private UserRoleDao userRoleDao;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private SecurityService securityService;

    @Transactional
    public void saveUser(User user) {
        userDao.save(user);
    }

    public void saveUser(String username, String password) {
        User newUser = new User();
        newUser.setUsername(username);
        newUser.setPassword(securityService.encodePassword(password));
        saveUser(newUser);
    }

    @Transactional
    public void updateUser(User user) {
        userDao.update(user);
    }

    @Transactional(readOnly = true)
    public List<User> getAllUsers() {
        return userDao.getAll();
    }

    @Transactional(readOnly = true)
    public List<UserDto> getAllUsersDto(){
        return getAllUsers().stream()
                .map(user -> convertToDto(user))
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public List<UserRole> getAllRoles() {
        return userRoleDao.getAll();
    }

    @Transactional(readOnly = true)
    public User findByUserName(String name){
        return userDao.findByUserName(name);
    }

    @Transactional
    public void deleteRole(String username, String role) {
        User user = userDao.findByUserName(username);
        Set<com.rjproject.entities.UserRole> userRoles = user.getUserRole();
        Iterator<com.rjproject.entities.UserRole> iterator = userRoles.iterator();
        while(iterator.hasNext()){
            UserRole tempRole = iterator.next();
            if (tempRole.getRole().equals(role)){
                iterator.remove();
                userRoleDao.delete(tempRole);
            }
        }
    }

    @Transactional
    public void addRole(String username, String role) {
        User user = userDao.findByUserName(username);
        UserUtilService.addSpecificUserRole(user,role);
        updateUser(user);
    }


    @Transactional(readOnly = true)
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = userDao.findByUserName(username);
        logger.info("this user tried to login >> " + user.getUsername());
        org.springframework.security.core.userdetails.User.UserBuilder builder = null;
        if (user != null) {

            builder = org.springframework.security.core.userdetails.User.withUsername(username);
            builder.disabled(!user.isEnabled());
            builder.password(user.getPassword());
            String[] authorities = user.getUserRole()
                    .stream().map(a -> a.getRole()).toArray(String[]::new);
            builder.authorities(authorities);
        } else {
            throw new UsernameNotFoundException("User not found.");
        }
        return builder.build();
    }

    @Override
    public UserDto convertToDto(User user) {
        return modelMapper.map(user, UserDto.class);
    }

    @Override
    public User convertToEntity(UserDto userDto) {
        return modelMapper.map(userDto, User.class);
    }

}