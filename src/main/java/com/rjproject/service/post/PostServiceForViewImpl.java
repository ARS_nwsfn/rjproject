package com.rjproject.service.post;

import com.rjproject.dto.PostCommentDto;
import com.rjproject.dto.PostDto;
import com.rjproject.entities.Post;
import com.rjproject.entities.PostComment;
import com.rjproject.service.security.SecurityService;
import com.rjproject.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class PostServiceForViewImpl implements PostServiceForView {

    @Autowired
    PostService postService;

    @Autowired
    UserService userService;

    @Autowired
    SecurityService securityService;

    @Override
    @Transactional
    public PostDto getPostById(int id) {
        return postService.convertPostToDto(postService.getPostById(id));
    }

    @Override
    @Transactional
    public PostDto createNewPost(String topic, String body, String username) {
        Post post = new Post();
        post.setTopic(topic);
        post.setPostBody(body);
        post.setUser(userService.findByUserName(username));
        post.setCreateDate(Calendar.getInstance());
        post.setTags(" ");
        postService.savePost(post);
        PostDto postDto = postService.convertPostToDto(post);
        return postDto;
    }

    public PostDto addComment(int postId, int parentCommentId, String comment){


        Post post = postService.getPostById(postId);
        PostComment newComment = new PostComment();
        newComment.setCreateDate(Calendar.getInstance());
        newComment.setPost(post);
        newComment.setUser(userService.findByUserName(securityService.getLoggedUsername()));
        newComment.setText(comment);
        newComment.setParentComment(postService.getCommentById(parentCommentId));

        postService.savePostComment(newComment);
        Set<PostComment> comments = post.getComments();
        comments.add(newComment);
        post.setComments(comments);

        return postService.convertPostToDto(post);
    }

    @Override
    @Transactional
    public List<PostDto> getPostsByPage(int page) {

        List<Post> posts = postService.getPostsByPage(page);
        return posts.stream()
                .map(post -> postService.convertPostToDto(post))
                .collect(Collectors.toList());
    }
    @Override
    @Transactional
    public List<PostDto> getPostsByUser(String username){
        List<Post> posts = postService.getPostsByUser(username);
        return posts.stream()
                .map(post -> postService.convertPostToDto(post))
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public List<PostCommentDto> getCommentsByUser(String username){
        List<PostComment> comments = postService.getCommentsByUser(username);
        return comments.stream()
                .map(comment -> postService.convertCommentToDto(comment))
                .collect(Collectors.toList());
    }
}
