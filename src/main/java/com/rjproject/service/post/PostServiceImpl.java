package com.rjproject.service.post;

import com.rjproject.dao.Post.PostCommentDao;
import com.rjproject.dao.Post.PostDao;
import com.rjproject.dao.Post.PostLikeDao;
import com.rjproject.dto.PostCommentDto;
import com.rjproject.dto.PostDto;
import com.rjproject.entities.Post;
import com.rjproject.entities.PostComment;
import com.rjproject.entities.PostLike;
import com.rjproject.service.security.SecurityService;
import com.rjproject.service.user.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PostServiceImpl implements PostService {

    @Autowired
    PostDao postDao;

    @Autowired
    PostLikeDao postLikeDao;

    @Autowired
    PostCommentDao postCommentDao;

    @Autowired
    SecurityService securityService;

    @Autowired
    UserService userService;


    @Autowired
    private ModelMapper modelMapper;

    @Override
    public void savePost(Post post) {
        postDao.save(post);
    }

    @Override
    public void updatePost(Post post) {
        postDao.update(post);
    }

    @Override
    public List<Post> getAllPosts() {
        return postDao.getAll();
    }

    @Override
    public List<Post> getPostsByPage(int page) {
        return postDao.getPostsByPage(page);
    }

    @Override
    public List<Post> getPostsByUser(String name){return postDao.getPostsByName(name);}

    @Override
    public long getTotalPostCount() {
        return postDao.getTotalPostCount();
    }

    @Override
    public Post getPostById(int id) {
        return postDao.getById(id);
    }

    @Override
    public void savePostLike(PostLike postLike) {
        postLikeDao.save(postLike);
    }

    @Override
    public void updatePostLike(PostLike postLike) {
        postLikeDao.update(postLike);
    }

    @Override
    public void changePostLike(PostLike like, int ratingChange) {
        if (ratingChange > 0) {
            like.setLike(true);
        } else {
            like.setLike(false);
        }
        updatePostLike(like);
    }

    @Override
    public void savePostComment(PostComment comment) {
        postCommentDao.save(comment);
    }

    @Override
    public void updatePostComment(PostComment comment) {
        postCommentDao.update(comment);
    }

    @Override
    public PostComment getCommentById(int id) {
        return postCommentDao.getById(id);
    }

    @Override
    public PostDto convertPostToDto(Post post) {
        return modelMapper.map(post, PostDto.class);
    }

    @Override
    public Post convertPostDtoToEntity(PostDto postDto) {
        return modelMapper.map(postDto, Post.class);
    }

    @Override
    public int likePost(int postId, int ratingChange) {

        Post post = getPostById(postId);
        String username = securityService.getLoggedUsername();
        PostLike currentLike = PostServiceUtil.getLikeIfLiked(post.getPostLikes(), username);
        int previousLike = 0;

        if (currentLike.getUser() == null) {
            currentLike.setUser(userService.findByUserName(username));
            currentLike.setPost(post);
            savePostLike(currentLike);
        } else {
            previousLike = currentLike.getLike() ? 1 : -1;
        }

        if (previousLike != ratingChange) {
            changePostLike(currentLike, ratingChange);
            post.setPostRating(post.getPostRating() + ratingChange);
            updatePost(post);
        }

        return post.getPostRating();
    }

    public PostCommentDto convertCommentToDto(PostComment comment){
        return modelMapper.map(comment, PostCommentDto.class);
    }

    public PostComment convertCommentToEntity(PostCommentDto commentDto){
        return modelMapper.map(commentDto, PostComment.class);
    }

    public  List<PostComment> getCommentsByUser(String username){
        return postCommentDao.getCommentsByName(username);
    }

}
