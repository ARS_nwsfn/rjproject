package com.rjproject.service.post;

import com.rjproject.dto.PostDto;
import com.rjproject.dto.PostLikeDto;
import com.rjproject.entities.Post;
import com.rjproject.entities.PostLike;
import com.rjproject.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Set;

public class PostServiceUtil {

    public static final int PAGE_POST_LIMIT = 4;
    public static final int PAGES_BACK_FORWARD_LIMIT = 3;

    public static Integer validatePageNumber(int currentPage, long totalPostNumber) {

        int lastPageNumber = (int) (Math.ceil(totalPostNumber / (float) PAGE_POST_LIMIT));
        if (currentPage < 1) {
            currentPage = 1;
        }
        if (currentPage > lastPageNumber) {
            currentPage = lastPageNumber;
        }
        return currentPage;
    }

    public static int getPaginationPageStart(int currentPage) {
        return (currentPage - PAGES_BACK_FORWARD_LIMIT > 1) ? (currentPage - PAGES_BACK_FORWARD_LIMIT ) : 1;
    }

    public static int getPaginationPageEnd(int currentPage, int lastPage) {
        return (currentPage + PAGES_BACK_FORWARD_LIMIT > lastPage) ? lastPage : (currentPage + PAGES_BACK_FORWARD_LIMIT);
    }

    public static PostLike getLikeIfLiked(Set<PostLike> likes, String name) {
        return likes.stream().filter(like -> like.getUser().getUsername().equals(name))
                             .findAny()
                             .orElse(new PostLike());
    }



}
