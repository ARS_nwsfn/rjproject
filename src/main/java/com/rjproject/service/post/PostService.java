package com.rjproject.service.post;

import com.rjproject.dto.PostCommentDto;
import com.rjproject.dto.PostDto;
import com.rjproject.entities.Post;
import com.rjproject.entities.PostComment;
import com.rjproject.entities.PostLike;

import java.util.List;

public interface PostService{

    void savePost(Post post);

    void updatePost(Post post);

    List<Post> getAllPosts();

    List<Post> getPostsByPage(int page);

    List<Post> getPostsByUser(String username);

    long getTotalPostCount();

    Post getPostById(int id);

    void savePostLike(PostLike postLike);

    void updatePostLike(PostLike postLike);

    void changePostLike(PostLike like, int ratingChange);

    void savePostComment(PostComment comment);

    void updatePostComment(PostComment comment);

    PostComment getCommentById(int id);

    PostDto convertPostToDto(Post post);

    Post convertPostDtoToEntity(PostDto postDto);

    int likePost(int postId, int ratingChange);

    PostCommentDto convertCommentToDto(PostComment comment);

    PostComment convertCommentToEntity(PostCommentDto comment);

    List<PostComment> getCommentsByUser(String username);

}
