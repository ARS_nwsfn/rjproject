package com.rjproject.service.post;

import com.rjproject.dto.PostCommentDto;
import com.rjproject.dto.PostDto;

import java.util.List;

public interface PostServiceForView {
    PostDto getPostById(int id);
    PostDto createNewPost(String topic, String body, String username);
    PostDto addComment(int postId, int parentCommentId, String comment);
    List<PostDto> getPostsByPage(int page);
    List<PostDto> getPostsByUser(String username);
    List<PostCommentDto> getCommentsByUser(String username);
}
