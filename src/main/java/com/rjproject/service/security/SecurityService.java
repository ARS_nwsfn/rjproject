package com.rjproject.service.security;

import com.rjproject.dto.UserDto;

public interface SecurityService {

    UserDto getLoggedInUser();
    void autoLogin(String username, String password);
    String getLoggedUsername();

    String encodePassword(String password);
}
