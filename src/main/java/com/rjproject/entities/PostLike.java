package com.rjproject.entities;


import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "POST_LIKE", uniqueConstraints = @UniqueConstraint(
        columnNames = { "POST", "USERNAME" }))
public class PostLike  {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "like_id",
            unique = true, nullable = false)
    private Integer postLikeId;

    @Column(name = "ISLIKE")
    private boolean isLike;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "USERNAME")
    private User user;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "POST")
    private Post post;

    public Integer getPostLikeId() {
        return postLikeId;
    }

    public void setPostLikeId(Integer postLikeId) {
        this.postLikeId = postLikeId;
    }

    public boolean getLike() {
        return isLike;
    }

    public void setLike(boolean like) {
        isLike = like;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }



}