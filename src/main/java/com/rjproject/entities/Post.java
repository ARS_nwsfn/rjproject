package com.rjproject.entities;

import com.fasterxml.jackson.annotation.JsonView;
import com.rjproject.jsonview.Views;

import javax.persistence.*;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "POSTS")
public class Post {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "post_id",
            unique = true, nullable = false)
    private Integer postId;

    @Column(name = "TOPIC", nullable = false)
    private String topic;

    @Column(name = "TAGS")
    private String tags;

    @Column(name = "POST_BODY", columnDefinition = "text")
    private String postBody;


    @Column(name = "POST_RATING")
    private Integer postRating = 0;


    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATE_DATE")
    private Calendar createDate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "USERNAME")
    private User user;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "post", fetch = FetchType.EAGER)
    private Set<PostLike> userPostLike = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "post", fetch = FetchType.LAZY)
    private Set<PostComment> comments = new HashSet<>();

    public Integer getPostId() {
        return postId;
    }

    public void setPostId(Integer postId) {
        this.postId = postId;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getPostBody() {
        return postBody;
    }

    public void setPostBody(String postBody) {
        this.postBody = postBody;
    }

    public Calendar getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Calendar createDate) {
        this.createDate = createDate;
    }

    public Integer getPostRating() {
        return postRating;
    }

    public void setPostRating(Integer postRating) {
        this.postRating = postRating;
    }

    public Set<PostLike> getPostLikes() {
        return userPostLike;
    }

    public void setPostLikes(Set<PostLike> userPostLike) {
        this.userPostLike = userPostLike;
    }

    public Set<PostComment> getComments() {
        return comments;
    }

    public void setComments(Set<PostComment> comments) {
        this.comments = comments;
    }

    @Override
    public String toString() {
        return "Post{" +
                "postId=" + postId +
                ", topic='" + topic + '\'' +
                ", tags='" + tags + '\'' +
                ", postRating=" + postRating +
                ", createDate=" + createDate +
                ", user=" + user +
                '}';
    }
}
