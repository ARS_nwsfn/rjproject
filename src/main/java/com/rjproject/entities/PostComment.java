package com.rjproject.entities;

import com.fasterxml.jackson.annotation.JsonView;
import com.rjproject.jsonview.Views;

import javax.persistence.*;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "COMMENTS")
public class PostComment {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "COMMENT_ID",
            unique = true, nullable = false)
    private Integer commentId;

    @Column(name = "TEXT", nullable = false)
    private String text;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATE_DATE")
    private Calendar createDate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "USERNAME")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "POST_ID")
    private Post post;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="PARRENT_COMMENT_ID")
    private PostComment parentComment;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "parentComment", fetch = FetchType.EAGER)
    private Set<PostComment> childComments = new HashSet<>();

    public Integer getCommentId() {
        return commentId;
    }

    public void setCommentId(Integer commentId) {
        this.commentId = commentId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public PostComment getParentComment() {
        return parentComment;
    }

    public void setParentComment(PostComment parentComment) {
        this.parentComment = parentComment;
    }

    public Set<PostComment> getChildComments() {
        return childComments;
    }

    public void setChildComments(Set<PostComment> childComments) {
        this.childComments = childComments;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public Calendar getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Calendar createDate) {
        this.createDate = createDate;
    }
}
