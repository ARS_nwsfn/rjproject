package com.rjproject.dto;


public class PostLikeDto{

    private Integer postLikeId;

    private boolean isLike;

    private UserDto user;

    private PostDto post;

    public Integer getPostLikeId() {
        return postLikeId;
    }

    public void setPostLikeId(Integer postLikeId) {
        this.postLikeId = postLikeId;
    }

    public boolean getLike() {
        return isLike;
    }

    public void setLike(boolean like) {
        isLike = like;
    }

    public UserDto getUser() {
        return user;
    }

    public void setUser(UserDto user) {
        this.user = user;
    }

    public PostDto getPost() {
        return post;
    }

    public void setPost(PostDto post) {
        this.post = post;
    }
}
