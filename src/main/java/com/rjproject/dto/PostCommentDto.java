package com.rjproject.dto;

import com.fasterxml.jackson.annotation.JsonView;
import com.rjproject.entities.Post;
import com.rjproject.entities.PostComment;
import com.rjproject.entities.User;
import com.rjproject.jsonview.Views;

import javax.persistence.*;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

public class PostCommentDto {

    @JsonView(Views.Public.class)
    private Integer commentId;

    @JsonView(Views.Public.class)
    private String text;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonView(Views.Public.class)
    private Calendar createDate;

    @JsonView(Views.Public.class)
    private UserDto user;

    private PostDto post;

    @JsonView(Views.Public.class)
    private PostCommentDto parentComment;

    private Set<PostCommentDto> childComments = new HashSet<>();

    public Integer getCommentId() {
        return commentId;
    }

    public void setCommentId(Integer commentId) {
        this.commentId = commentId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public UserDto getUser() {
        return user;
    }

    public void setUser(UserDto user) {
        this.user = user;
    }

    public PostCommentDto getParentComment() {
        return parentComment;
    }

    public void setParentComment(PostCommentDto parentComment) {
        this.parentComment = parentComment;
    }

    public Set<PostCommentDto> getChildComments() {
        return childComments;
    }

    public void setChildComments(Set<PostCommentDto> childComments) {
        this.childComments = childComments;
    }

    public PostDto getPost() {
        return post;
    }

    public void setPost(PostDto post) {
        this.post = post;
    }

    public Calendar getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Calendar createDate) {
        this.createDate = createDate;
    }
}
