package com.rjproject.dto;

import com.fasterxml.jackson.annotation.JsonView;
import com.rjproject.jsonview.Views;

import java.util.HashSet;
import java.util.Set;

public class UserDto{

    @JsonView(Views.Public.class)
    private String username;

    private boolean enabled = true;

    private Set<UserRoleDto> userRole = new HashSet<>();

    private Set<PostDto> userPost = new HashSet<>();

    private Set<PostLikeDto> userPostLike = new HashSet<>();

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Set<UserRoleDto> getUserRole() {
        return userRole;
    }

    public void setUserRole(Set<UserRoleDto> userRole) {
        this.userRole = userRole;
    }

    public Set<PostDto> getUserPost() {
        return userPost;
    }

    public void setUserPost(Set<PostDto> userPost) {
        this.userPost = userPost;
    }

    public Set<PostLikeDto> getUserPostLike() {
        return userPostLike;
    }

    public void setUserPostLike(Set<PostLikeDto> userPostLike) {
        this.userPostLike = userPostLike;
    }

    public String toStringAllRoles() {
        String roles = " ";
        for (UserRoleDto role : userRole) {
            roles = roles.concat(role.getRole().toString()).concat(" ");
        }
        return roles;
    }


}
