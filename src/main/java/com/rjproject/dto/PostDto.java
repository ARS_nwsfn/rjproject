package com.rjproject.dto;

import com.fasterxml.jackson.annotation.JsonView;
import com.rjproject.jsonview.Views;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

public class PostDto {

    @JsonView(Views.Public.class)
    private Integer postId;

    @JsonView(Views.Public.class)
    private String topic;

    private String tags;

    @JsonView(Views.Public.class)
    private String postBody;

    @JsonView(Views.Public.class)
    private Integer postRating = 0;


    @Temporal(TemporalType.TIMESTAMP)
    private Calendar createDate;

    private UserDto user;

    private Set<PostLikeDto> userPostLike = new HashSet<>();

    private Set<PostCommentDto> comments = new HashSet<>();

    public Integer getPostId() {
        return postId;
    }

    public void setPostId(Integer postId) {
        this.postId = postId;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public UserDto getUser() {
        return user;
    }

    public void setUser(UserDto user) {
        this.user = user;
    }

    public String getPostBody() {
        return postBody;
    }

    public void setPostBody(String postBody) {
        this.postBody = postBody;
    }

    public Calendar getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Calendar createDate) {
        this.createDate = createDate;
    }

    public Integer getPostRating() {
        return postRating;
    }

    public void setPostRating(Integer postRating) {
        this.postRating = postRating;
    }

    public Set<PostLikeDto> getPostLikes() {
        return userPostLike;
    }

    public void setPostLikes(Set<PostLikeDto> userPostLike) {
        this.userPostLike = userPostLike;
    }

    public Set<PostCommentDto> getComments() {
        return comments;
    }

    public void setComments(Set<PostCommentDto> comments) {
        this.comments = comments;
    }
}
