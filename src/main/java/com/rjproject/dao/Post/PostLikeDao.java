package com.rjproject.dao.Post;

import com.rjproject.dao.CrudDao;
import com.rjproject.entities.PostLike;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class PostLikeDao extends CrudDao<PostLike> {

    public PostLikeDao(){
        super(PostLike.class);
    }

}
