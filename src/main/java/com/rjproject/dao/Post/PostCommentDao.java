package com.rjproject.dao.Post;

import com.rjproject.dao.CrudDao;
import com.rjproject.entities.PostComment;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.TypedQuery;
import java.util.List;

@Repository
@Transactional
public class PostCommentDao extends CrudDao<PostComment> {

    public PostCommentDao(){
        super(PostComment.class);
    }

    public List<PostComment> getCommentsByName(String name){
        String hql = "select c from PostComment c where c.user.username = :name";
        TypedQuery<PostComment> query = sessionFactory.getCurrentSession().createQuery(hql)
                .setParameter("name",name);
        return  query.getResultList();
    }
}
