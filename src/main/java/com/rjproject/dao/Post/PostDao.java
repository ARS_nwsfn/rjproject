package com.rjproject.dao.Post;

import com.rjproject.dao.CrudDao;
import com.rjproject.entities.Post;
import com.rjproject.service.post.PostServiceUtil;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.TypedQuery;
import java.util.List;

@Repository
@Transactional
public class PostDao extends CrudDao<Post> {

    public PostDao(){
        super(Post.class);
    }

    public List<Post> getPostsByPage(int page) {
        int firstPostOnPage;
        if (page > 1) {
            firstPostOnPage = PostServiceUtil.PAGE_POST_LIMIT * (page - 1);
        } else {
            firstPostOnPage = 0;
        }
        TypedQuery<Post> query = sessionFactory.getCurrentSession().createQuery("from Post");
        query.setFirstResult(firstPostOnPage);
        query.setMaxResults(PostServiceUtil.PAGE_POST_LIMIT);
        return query.getResultList();
    }

    public long getTotalPostCount() {
        String countQ = "Select count (p.postId) from Post p";
        Query countQuery = sessionFactory.getCurrentSession().createQuery(countQ);
        Long countResults = (Long) countQuery.uniqueResult();
        return countResults;
    }

    public List<Post> getPostsByName(String name){
        String hql = "select p from Post p where p.user.username = :name";
        TypedQuery<Post> query = sessionFactory.getCurrentSession().createQuery(hql)
        .setParameter("name",name);
        return  query.getResultList();
    }

}
