package com.rjproject.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;


public abstract class CrudDao<T> {

    private Class<T> type;

    public CrudDao(Class<T> type){
        this.type = type;
    }

    @Autowired
    protected SessionFactory sessionFactory;

    @Transactional
    public void save(T entity) {
        sessionFactory.getCurrentSession().save(entity);
    }

    @Transactional
    public void update(T entity) {
        sessionFactory.getCurrentSession().merge(entity);
    }

    @Transactional
    public void delete(T entity) {
        sessionFactory.getCurrentSession().delete(entity);
    }

    @Transactional
    public List<T> getAll() {

        CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
        CriteriaQuery<T> criteria = builder.createQuery(type);
        criteria.from(type);
        TypedQuery<T> query = sessionFactory.getCurrentSession().createQuery(criteria);
        return query.getResultList();
    }

    @Transactional
    public T getById(int Id) {
        return sessionFactory.getCurrentSession().get(type, Id);
    }

}
