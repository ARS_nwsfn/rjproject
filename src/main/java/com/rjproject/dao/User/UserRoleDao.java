package com.rjproject.dao.User;

import com.rjproject.dao.CrudDao;
import com.rjproject.entities.UserRole;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class UserRoleDao extends CrudDao<UserRole> {

    public UserRoleDao(){
        super(UserRole.class);
    }

}
