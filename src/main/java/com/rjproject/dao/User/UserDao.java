package com.rjproject.dao.User;

import com.rjproject.dao.CrudDao;
import com.rjproject.entities.User;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository
@Transactional
public class UserDao extends CrudDao<User> {

    public UserDao(){
        super(User.class);
    }

    public User findByUserName(String username) {
        return sessionFactory.getCurrentSession().get(User.class, username);
    }

}