package com.rjproject;

import com.rjproject.entities.Post;
import com.rjproject.entities.PostComment;
import com.rjproject.entities.PostLike;
import com.rjproject.entities.User;
import com.rjproject.service.post.PostService;
import com.rjproject.service.user.UserUtilService;
import com.rjproject.service.user.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;

@Service
public class RunJob implements InitializingBean {

    static final String POST_BODY_A = "Java is a general-purpose programming language " +
            "that is class-based, object-oriented, and designed to have as few implementation" +
            " dependencies as possible. It is intended to let application developers \"write once, run anywhere\" (WORA)," +
            " meaning that compiled Java code can run on all platforms that support Java without the need for recompilation. " +
            "Java applications are typically compiled to \"bytecode\" that can run on any Java virtual machine (JVM) regardless of the underlying computer architecture." +
            " The syntax of Java is similar to C and C++, but it has fewer low-level facilities than either of them. As of 2018," +
            " Java was one of the most popular programming languages in use according to GitHub, particularly for client-server web applications," +
            " with a reported 9 million developers.";

    static final String POST_BODY_B = "Java was originally developed by James Gosling at Sun Microsystems (which has since been acquired by Oracle)" +
            " and released in 1995 as a core component of Sun Microsystems' Java platform. The original and reference implementation Java compilers," +
            " virtual machines, and class libraries were originally released by Sun under proprietary licenses. As of May 2007, in compliance with the specifications " +
            "of the Java Community Process, Sun had relicensed most of its Java technologies under the GNU General Public License. Meanwhile, others have developed" +
            " alternative implementations of these Sun technologies, such as the GNU Compiler for Java (bytecode compiler), GNU Classpath (standard libraries), " +
            "and IcedTea-Web (browser plugin for applets).";

    private static final Logger logger = Logger.getLogger(RunJob.class);

    @Autowired
    private UserService userService;

    @Autowired
    private PostService postService;


    public void afterPropertiesSet() {


        User userA = new User();
        userA.setUsername("admin");
        userA.setPassword(UserUtilService.encodePassword("123"));
        UserUtilService.addSpecificUserRole(userA, "ADMIN");
        UserUtilService.addSpecificUserRole(userA, "USER");
        UserUtilService.addSpecificUserRole(userA, "USER");
        userService.saveUser(userA);
        logger.info("user " + userA.getUsername() + " is saved with " + userA.toStringAllRoles() + " authorities");

        User userB = new User();
        userB.setUsername("user");
        userB.setPassword(UserUtilService.encodePassword("123"));
        UserUtilService.addSpecificUserRole(userB, "USER");
        userService.saveUser(userB);
        logger.info("user " + userB.getUsername() + " is saved with " + userB.toStringAllRoles() + " authorities");

        User userC = new User();
        userC.setUsername("ars");
        userC.setPassword(UserUtilService.encodePassword("123"));
        UserUtilService.addSpecificUserRole(userC, "USER");
        UserUtilService.addSpecificUserRole(userC, "BADROLE");
        userService.saveUser(userC);
        logger.info("user " + userC.getUsername() + " is saved with " + userC.toStringAllRoles() + " authorities");

        Post postA = new Post();
        postA.setTopic("my first post yay");
        postA.setUser(userA);
        postA.setPostBody(POST_BODY_A);
        postA.setTags("post, new post, strange post");
        postA.setCreateDate(Calendar.getInstance());
        postA.setPostRating(13);
        postService.savePost(postA);

        // set first post is liked for admin
        PostLike likeA = new PostLike();
        likeA.setLike(true);
        likeA.setUser(userA);
        likeA.setPost(postA);
        postService.savePostLike(likeA);
        // set some comments to first post
        PostComment parentCommentA = new PostComment();
        parentCommentA.setText("i'm parent comment A");
        parentCommentA.setUser(userA);
        parentCommentA.setPost(postA);
        parentCommentA.setCreateDate(Calendar.getInstance());
        postService.savePostComment(parentCommentA);

        PostComment parentCommentB = new PostComment();
        parentCommentB.setText("i'm parent comment B");
        parentCommentB.setUser(userA);
        parentCommentB.setPost(postA);
        parentCommentB.setCreateDate(Calendar.getInstance());
        postService.savePostComment(parentCommentB);

        PostComment childCommentAB = new PostComment();
        childCommentAB.setText("i'm child comment AB of A");
        childCommentAB.setUser(userA);
        childCommentAB.setPost(postA);
        childCommentAB.setParentComment(parentCommentA);
        childCommentAB.setCreateDate(Calendar.getInstance());
        postService.savePostComment(childCommentAB);

        PostComment childCommentABC = new PostComment();
        childCommentABC.setText("i'm child comment ABC of AB");
        childCommentABC.setUser(userA);
        childCommentABC.setPost(postA);
        childCommentABC.setParentComment( childCommentAB);
        childCommentABC.setCreateDate(Calendar.getInstance());
        postService.savePostComment(childCommentABC);
        //


        Post postB = new Post();
        postB.setTopic("my second post kek");
        postB.setUser(userA);
        postB.setPostBody(POST_BODY_B);
        postB.setTags("post, next post, strange post");
        postB.setCreateDate(Calendar.getInstance());
        postService.savePost(postB);

        // set second post disliked for admin
        PostLike likeB = new PostLike();
        likeB.setLike(false);
        likeB.setUser(userA);
        likeB.setPost(postB);
        postService.savePostLike(likeB);
        //

        Post postC = new Post();
        postC.setTopic("my third post kek");
        postC.setUser(userA);
        postC.setPostBody(POST_BODY_B);
        postC.setTags("post, next post, strange post");
        postC.setCreateDate(Calendar.getInstance());
        postService.savePost(postC);

        postC.setTopic("my fourth post kek");
        postC.setUser(userA);
        postC.setPostBody(POST_BODY_B);
        postC.setTags("post, next post, strange post");
        postC.setCreateDate(Calendar.getInstance());
        postService.savePost(postC);

        postC.setTopic("my fifth post kek");
        postC.setUser(userB);
        postC.setPostBody(POST_BODY_B);
        postC.setTags("post, next post, strange post");
        postC.setCreateDate(Calendar.getInstance());
        postService.savePost(postC);

        postB.setTopic("my six post kek");
        postB.setUser(userB);
        postB.setPostBody(POST_BODY_B);
        postB.setTags("post, next post, strange post");
        postC.setCreateDate(Calendar.getInstance());
        postService.savePost(postB);

        postB.setTopic("my seven post kek");
        postB.setUser(userB);
        postB.setPostBody(POST_BODY_B);
        postB.setTags("post, next post, strange post");
        postC.setCreateDate(Calendar.getInstance());
        postService.savePost(postB);

        postB.setTopic("my eight post kek");
        postB.setUser(userB);
        postB.setPostBody(POST_BODY_B);
        postB.setTags("post, next post, strange post");
        postC.setCreateDate(Calendar.getInstance());
        postService.savePost(postB);

        postB.setTopic("my nine post kek");
        postB.setUser(userC);
        postB.setPostBody(POST_BODY_B);
        postB.setTags("post, next post, strange post");
        postC.setCreateDate(Calendar.getInstance());
        postService.savePost(postB);

        postB.setTopic("my ten post kek");
        postB.setUser(userC);
        postB.setPostBody(POST_BODY_B);
        postB.setTags("post, next post, strange post");
        postC.setCreateDate(Calendar.getInstance());
        postService.savePost(postB);


    }
}
