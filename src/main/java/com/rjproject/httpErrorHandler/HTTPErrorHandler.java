package com.rjproject.httpErrorHandler;

import com.rjproject.controllers.login.LoginController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("*")
public class HTTPErrorHandler {

//    @RequestMapping(value = "/404")
//    public String error404(HttpServletRequest request, HttpServletResponse response) throws IOException {
//        return "/login";
//
//
//    }
//
//    @RequestMapping(value = "/400")
//    public String error400(HttpServletRequest request, HttpServletResponse response) throws IOException {
//
//        return "redirect:/login";
//    }

    Logger logger = LoggerFactory.getLogger(LoginController.class);

    // for 403 access denied page
    @RequestMapping(value = "/403", method = RequestMethod.GET)
    public ModelAndView accesssDenied() {

        ModelAndView model = new ModelAndView();

        // check if user is login
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (!(auth instanceof AnonymousAuthenticationToken)) {
            UserDetails userDetail = (UserDetails) auth.getPrincipal();
            System.out.println(userDetail);

            model.addObject("username", userDetail.getUsername());
            logger.debug("acces denied page shown to " + userDetail.getUsername());
        }

        model.setViewName("403");
        return model;

    }

}
