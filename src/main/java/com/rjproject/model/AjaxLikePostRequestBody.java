package com.rjproject.model;

public class AjaxLikePostRequestBody {

    private int rating;

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }
}
