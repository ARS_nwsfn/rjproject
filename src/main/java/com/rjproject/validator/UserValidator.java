package com.rjproject.validator;

import com.rjproject.dao.User.UserDao;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class UserValidator implements Validator {

    private static final Logger logger = Logger.getLogger(UserValidator.class);

    @Autowired
    private UserDao userDao;

    @Override
    public boolean supports(Class<?> aClass) {
        return UserForm.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        UserForm user = (UserForm) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "Required","username field is required");
        if (user.getUsername().length() < 3 || user.getUsername().length() > 32) {
            errors.rejectValue("username", "Size.userForm.username","username field must have at least 3 symbols");
            logger.info(errors.getFieldError("username"));
        }

        if (userDao.findByUserName(user.getUsername()) != null) {
            errors.rejectValue("username", "Duplicate.userForm.username","username already exist");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "Required","password is required");
        if (user.getPassword().length() < 3 || user.getPassword().length() > 32) {
            errors.rejectValue("password", "Size.userForm.password","password field must have at least 3 symbols");
        }

        if (!user.getConfirmPassword().equals(user.getPassword())) {
            errors.rejectValue("confirmPassword", "Different.userForm.password","password field doesn't match password confirmation field");
        }
    }
}