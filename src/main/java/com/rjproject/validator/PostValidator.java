package com.rjproject.validator;

import com.rjproject.dao.Post.PostDao;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class PostValidator implements Validator {

    private static final Logger logger = Logger.getLogger(PostValidator.class);

    @Autowired
    private PostDao postDao;

    @Override
    public boolean supports(Class<?> aClass) {
        return PostForm.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        PostForm post = (PostForm) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "topic", "Required","post topic is required");
        if (post.getTopic().length() < 3 || post.getTopic().length() > 60) {
            errors.rejectValue("topic", "Size.postTopic.topic","post topic field must have at least 3 symbols");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "postBody", "Required","Post body shouldn't be empty");
    }
}
