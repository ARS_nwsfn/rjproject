jQuery(document).ready(function($) {

    $("#loadPosts,#loadComments").click(function(event) {

        var sender = $(this);
        event.preventDefault();
        var url = sender.attr('href');
        $("#resultBlock").load(url);
    });
});