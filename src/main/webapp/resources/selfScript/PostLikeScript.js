jQuery(document).ready(function($) {

    $(".button-plus").click(function(event) {

        var sender = $(this);
        var rating_elem = sender.parent().find(".post_rating");
        var dislike_button = sender.parent().find(".button-minus-active");
        event.preventDefault();
        var postId = this.dataset.post_id;
        likePost(postId,rating_elem,1);
        sender.addClass('button-plus-active');
        dislike_button.removeClass('button-minus-active');
    });

});

jQuery(document).ready(function($) {

    $(".button-minus").click(function(event) {

        var sender = $(this);
        var rating_elem = sender.parent().find(".post_rating");
        var like_button = sender.parent().find(".button-plus-active");
        event.preventDefault();
        var postId = this.dataset.post_id;
        likePost(postId,rating_elem,-1);
        sender.addClass('button-minus-active');
        like_button.removeClass('button-plus-active');
    });

});

function likePost(postId,ratingElem, ratingChange) {

    var search = {}
    search["rating"] = ratingChange;

    $.ajax({
        type : "POST",
        contentType : 'application/json; charset=utf-8',
        dataType : 'json',
        url : "/api/post/" + postId + "/like",
        data : JSON.stringify(search),
        timeout : 100000,
        success : function(data) {
            changeRating(ratingElem,data.result);
        },
        error : function(e) {
            changeRating(ratingElem,'error');
        },
        done : function(e) {
            enableSearchButton(true);
        }
    });

}

function changeRating(rating_elem,newRate){
    rating_elem.html(newRate);
}