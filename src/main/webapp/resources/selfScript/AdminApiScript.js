jQuery(document).ready(function($) {

    $(".btn-delete").click(function(event) {

        event.preventDefault();
        var href = this.href;
        editRole("DELETE",href);
    });

    $(".btn-add").click(function(event) {

            event.preventDefault();
            var href = this.href;
            editRole("PUT",href);
        });

});

function  editRole(mode, url) {
  return fetch(url, {
    method: mode
  }).then(response => window.location.href = response .url)
}