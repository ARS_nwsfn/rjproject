jQuery(document).ready(function($) {

    let commentModule = new Comments({ element: document.querySelector('#comment-section') });
});

class Comments {
  constructor({element}) {
    this._inputShown = false;
    document.addEventListener('click', e => {
            if(!e.target.closest('.button-comment-response')) return
            this.addReplyField(e.target.parentElement.dataset.comment_id);
    });
    document.addEventListener('click', e => {
                  if(!e.target.closest('.button-send-comment')) return
                  this.createComment(e.target.parentElement.parentElement.dataset.comment_id, e.target.previousElementSibling.value, postId);
                  e.target.previousElementSibling.value = "";
          });
    this._el = element;

    this.getComments(postId);
  }

  _render() {
    this._el.innerHTML = `
      ${this._comments.map(com => {
          if(com.parentComment) return;

        return findChildren(this._comments, com);
      }).join('')}
    `;
  }

  addReplyField(id) {
    this._lastId = this._lastId || id;
    this._shouldShow = this._lastId === id ? !this._shouldShow : true
    this._render();
    if(this._shouldShow) {
    document.querySelector(`[data-comment_id="${id}"] .button-comment-response`)
      .insertAdjacentHTML("afterend", `
                            <div class='comment-input-section'>
                                <input type="comment"  name="comment" placeholder="put your comment here" class='comment-input'>
                                    <button class='button-send-comment'> send </button>
                            </div>`);
    this._lastId = id;
    } else this._lastId = true;
  }

  async getComments(postId) {
    let result = await fetch("/api/post/"+ postId + "/comments")
    this._comments = await result.json();
    this._comments = sortComments(this._comments.result);
    this._render();
  }

  async createComment(commentId, commentText, postId) {
    let search = {
      commentId,
      commentText,
    }

    let result = await fetch("/api/post/" + postId + "/add-comment", {
      method: "POST",
      body: JSON.stringify(search),
      headers: {
              'Content-Type': 'application/json',
            }
    })

   this._comments = await result.json();
   this._comments = sortComments(this._comments.result);
   this._render();
  }
};

const findChildren = (array, elem, result = makeComment(elem), children = array.filter(el => el.parentComment ? el.parentComment.commentId === elem.commentId : false)) =>
  (children.length) ? closeComment(result + children.map(child => closeComment(findChildren(array, child))).join('')) :
  closeComment(result);

// sort comments in date order
const sortComments = commArr => commArr.sort((a, b) => +a.createDate - b.createDate);
// convert calendar into date
function getFullDate(dateString) {
            let date = new Date(dateString);
            return `${date.getDate()}.${date.getMonth()}.${date.getFullYear()} ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`
        }

const makeComment = comment => `
<div class="comment" data-comment_id = '${comment.commentId}'>
<a class="author" href=/user/${comment.user.username}>${comment.user.username}</a> ${getFullDate(comment.createDate)}
  <div class="comment-text">${comment.text}</div>
  ${!document.querySelector('[data-user_logged_in="anonymousUser"]') ?
        '<button class="button-comment-response"> response </button>'          : ''}

`;

const closeComment = comment => comment.match(/<\/div/g) ?
      `${comment}${'</div>'.repeat(comment.match(/<div/g).length - comment.match(/<\/div/g).length)}` : `${comment}${'</div>'.repeat(comment.match(/<div/g).length)}`